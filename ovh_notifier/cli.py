import logging
import typer
from pathlib import Path
from ovh_notifier.ovh.availability import OVHServerAvailability, OVHServerFilter
from ovh_notifier.notifier import notifiers
import time
import configparser

running = True
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(filename)s:%(lineno)d - %(levelname)s: %(message)s",
)
logger = logging.getLogger("ovh-notifier")


app = typer.Typer()


@app.command()
def server_identifier_lookup(name: str):
    print(OVHServerAvailability().get_server_identifier(name))


@app.command()
def check_availability(
    servers: list[str],
    config_file: Path = None,
    interval: int = 3600,
    verbose: bool = False,
):
    config = configparser.ConfigParser()
    config.read_dict(
        {
            "api": {"interval": interval},
            "notifier": {"kind": "desktop"},
            "notifier_params": {},
        }
    )
    if config_file and config_file.exists():
        config.read(config_file)
    selected_notifiers = []
    print(config)

    builder = notifiers[config["notifier"]["kind"]]
    selected_notifiers.append(builder(**config["notifier_params"]))
    if verbose:
        logger.setLevel(logging.DEBUG)
    logger.debug(f"check_availability: servers:{servers} {config}{notifiers}{verbose}")
    previous = None
    current = None
    filter = []
    for server in servers:
        filter.append(OVHServerFilter(server))
    checker = OVHServerAvailability()
    logger.info(
        f"Start checking for {servers} every {interval}s and notify on {','.join([type(notifier).__name__ for notifier in selected_notifiers])}"
    )
    while True:
        logger.info(f"Run check with {', '.join([str(x) for x in filter])}")
        try:
            previous = current
            current = checker.get_availability(filter)
            logger.debug(f"Found availability: {current}")
            for notifier in selected_notifiers:
                logger.debug(f"Call notifier {notifier}")
                notifier.report_status(previous, current)
        except Exception as _exc:
            logger.exception("Cannot get server info")

        logger.info(f"Wait for {interval}s")
        time.sleep(float(interval))


def main():
    logger.info("--------------------")
    app()


if __name__ == "__main__":
    main()
