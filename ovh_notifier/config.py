import configparser
import logging
from pathlib import Path

logger = logging.getLogger("config")


def load_config(config_file: Path):
    config = configparser.ConfigParser()
    config = {"api": {"polling": 60}}

    if config_file is not None:
        if not config_file.exists():
            raise Exception(
                f"Configuration file does not exists. {config_file}: no such file or directory"
            )

        config.read(config_file)
    return config
