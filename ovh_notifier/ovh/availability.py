from dataclasses import dataclass, field
import requests
import logging
from bs4 import BeautifulSoup
from ovh_notifier.ovh.status import AvailabilitySummary, AvailabilityServer

logger = logging.getLogger("availability")


@dataclass
class OVHServerFilter:
    identifier: str  # 22sk010
    datacenters: set[str] = field(default_factory=set)

    def __str__(self):
        if len(self.datacenters) > 0:
            return f"{self.identifier}: {', '.join(self.datacenters)}"
        else:
            return f"{self.identifier}: any"


class OVHServerAvailability:
    def __init__(self, endpoint="www.ovh.com"):
        self.api_url = (
            f"https://{endpoint}/engine/api/dedicated/server/datacenter/availabilities"
        )
        self.session = requests.Session()

    def get_server_identifier(self, name):
        reply = self.session.get(
            "https://eco.ovhcloud.com/fr/", allow_redirects=True, timeout=10
        )
        soup = BeautifulSoup(reply.text, "html.parser")
        return (
            soup.select(".ods-card--all-server[data-product-id=KS-1]")[0]
            .select("div[data-plancode]")[0]
            .attrs["data-plancode"]
        )

    def get_server_identifiers(self):
        response = self.query(None)[0]
        servers = []
        for server in response:
            servers.append(server["planCode"])
        return servers

    def get_availability(self, filter: list[OVHServerFilter] = None):
        status = AvailabilitySummary()
        data = self.query(filter)
        for server in data:
            datacenter_status = {}
            logger.debug(f"server: {server}")
            server_name = server["planCode"]
            for dc in server["datacenters"]:
                logger.debug(f"{server_name}-{dc['datacenter']}: {dc['availability']}")
                datacenter_status.update(
                    {dc["datacenter"]: dc["availability"] != "unavailable"}
                )
            server_availability = AvailabilityServer(
                server_name, avaibility=datacenter_status
            )
            status.add_server(server_name, server_availability)
        logger.debug(f"Status: {status}")
        return status

    def query(self, filter: list[OVHServerFilter] = None):
        params = {}
        responses = []
        if filter:
            for server in filter:
                params = {"server": server.identifier}
                if server.datacenters:
                    params = {"datacenters": ",".join(server.datacenters)}
                responses += self.session.get(self.api_url, params=params).json()
        else:
            responses = self.session.get(self.api_url, params=params).json()
        logger.debug(f"Got response: {responses}")
        return responses
