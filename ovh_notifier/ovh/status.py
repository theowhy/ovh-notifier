from dataclasses import dataclass, field
import functools
import logging

logger = logging.getLogger("status")


@dataclass
class AvailabilityServer:
    identifier: str
    avaibility: dict[str, bool]
    name: str = None

    @property
    def is_available(self):
        return functools.reduce(lambda x, y: x or y, self.avaibility.values(), False)


@dataclass
class AvailabilitySummary:
    servers_status: dict[str, AvailabilityServer] = field(default_factory=dict)

    def add_server(self, server, datacenters):
        self.servers_status.update({server: datacenters})

    @property
    def servers_summary(self):
        for name, server in self.servers_status.items():
            yield (name, server.is_available)

    def __str__(self):
        return str(list(self.servers_summary))
