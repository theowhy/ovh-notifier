from .desktop import DesktopNofitificationNotifier
from .email import EmailNotifier

notifiers = {"desktop": DesktopNofitificationNotifier, "email": EmailNotifier}
