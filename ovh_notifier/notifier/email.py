import smtplib
from email.message import EmailMessage
from ovh_notifier.notifier.base import Reporter
from dataclasses import dataclass
import logging

logger = logging.getLogger("notifer-smtp")


@dataclass
class EmailNotifier(Reporter):
    server: str
    email_from: str
    email_to: str
    username: str
    password: str
    tls: bool = True
    port: int = 25

    def notify(self, title: str, message: str):
        logger.debug("Sending Email")
        # smtp_to = config.get(utils.SECTION_EMAIL_NAME, utils.EMAIL_SMTP_TO_NAME)
        msg = EmailMessage()
        msg["From"] = self.email_from
        msg["To"] = self.email_to
        msg["Subject"] = title
        msg.set_content(message)
        if self.tls:
            mailserver = smtplib.SMTP_SSL(self.server, self.port)
        else:
            mailserver = smtplib.SMTP(self.server)
        if self.username and self.password:
            mailserver.login(self.username, self.password)
        mailserver.send_message(msg)
        mailserver.quit()
