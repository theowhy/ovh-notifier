from abc import ABC, abstractmethod
from ovh_notifier.ovh.status import AvailabilitySummary
import logging

logger = logging.getLogger("notifier.base")


class Reporter(ABC):
    def report_status(
        self, previous: AvailabilitySummary, current: AvailabilitySummary
    ):
        for server, available in current.servers_summary:
            was_available = (
                dict(previous.servers_summary)[server] if previous else False
            )
            if available:
                self.notify(
                    title="OVH server available",
                    message=f"Server {server} is available",
                )
            elif was_available:
                self.notify(
                    title="OVH server is not available anymore",
                    message=f"Server {server} is not available anymore",
                )
            else:
                logger.info(f"Server {server} is still not available")

    @abstractmethod
    def notify(self, title: str, message: str):
        pass
