from desktop_notifier import DesktopNotifier, Urgency
from ovh_notifier.notifier.base import Reporter


class DesktopNofitificationNotifier(Reporter):
    def __init__(self, **args):
        self.notifier = DesktopNotifier()

    def notify(self, title: str, message: str):
        self.notifier.send_sync(
            title=title,
            message=message,
            urgency=Urgency.Critical,
        )
